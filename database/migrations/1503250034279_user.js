/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class UserSchema extends Schema {
  up() {
    this.create('users', table => {
      table.increments();
      table.string('name').notNullable();
      table.string('bio');
      table.string('title');

      //   table
      //     .integer('avatar_id')
      //     .unsigned()
      //     .references('id')
      //     .inTable('files')
      //     .onDelete('SET NULL')
      //     .onUpdate('CASCADE');

      table.string('avatar');
      table.string('github');
      table.string('linkedin');
      table
        .string('email')
        .notNullable()
        .unique();
      table.string('password').notNullable();
      table.timestamps();
    });
  }

  down() {
    this.drop('users');
  }
}

module.exports = UserSchema;
