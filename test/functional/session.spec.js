const { test, trait } = use('Test/Suite')('Session');

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory');

trait('Test/ApiClient');
trait('DatabaseTransactions');

test('it should return JWT', async ({ assert, client }) => {
    const sessionPayload = {
        email: 'anderson@servidor.com',
        password: '123456',
    };

    await Factory.model('App/Models/User').create(sessionPayload);

    // await User.create({
    //     name: 'Anderson',
    //     email: 'anderson@servidor.com',
    //     password: '123456'
    // })

    const response = await client
        .post('/sessions')
        .send(sessionPayload)
        .end();

    response.assertStatus(200);
    assert.exists(response.body.token);
});
