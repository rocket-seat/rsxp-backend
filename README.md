adonis serve --dev

adonis test

adonis key:generate
adonis make:controller Session

yarn eslint --fix app --ext .js
yarn eslint --fix test --ext .js
yarn eslint --fix start --ext .js
yarn eslint --fix database --ext .js
yarn eslint --fix config --ext .js

docker run --name rsxp  -p 5432:5432 -d -t kartoza/postgis

docker start rsxp

2h:03